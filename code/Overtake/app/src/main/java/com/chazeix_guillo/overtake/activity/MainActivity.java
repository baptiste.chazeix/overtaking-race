package com.chazeix_guillo.overtake.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.chazeix_guillo.overtake.R;

public class MainActivity extends AppCompatActivity{

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.home_page);

        sharedPreferences = getBaseContext().getSharedPreferences("sp", MODE_PRIVATE);

        if (sharedPreferences.contains("music")){

            boolean music = sharedPreferences.getBoolean("music",true);

        } else {
            sharedPreferences
                    .edit()
                    .putBoolean("music",true)
                    .apply();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void play(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    public void reward(View view) {
        startActivity(new Intent(this, RewardActivity.class));
    }

    public void credits(View view) {
        startActivity(new Intent(this, CreditsActivity.class));
    }
}
