package com.chazeix_guillo.overtake.interfaces;

public interface IGameView {
    void draw();
}
