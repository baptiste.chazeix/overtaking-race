package com.chazeix_guillo.overtake.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.chazeix_guillo.overtake.R;
import com.chazeix_guillo.overtake.controler.GameControler;
import com.chazeix_guillo.overtake.controler.MusicManager;
import com.chazeix_guillo.overtake.controler.SensorControler;
import com.chazeix_guillo.overtake.interfaces.GameListener;
import com.chazeix_guillo.overtake.view.GameView;

public class GameActivity extends AppCompatActivity implements GameListener {

    private SensorControler sensor;
    private GameControler gameControler;
    private GameView gv;
    private RelativeLayout r;
    private MusicManager mv;
    private boolean wentOnPause;
    SharedPreferences sharedPreferences;

    public static final Object sync = new Object();

    static GameActivity gameActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameActivity = this;

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        sensor = new SensorControler((SensorManager) getSystemService(SENSOR_SERVICE));

        r = new RelativeLayout(this);
        gameControler = new GameControler(this, sensor, this);
        gv = new GameView(this, gameControler);
        gameControler.setGameView(gv);
        r.addView(gv);
        Button b = new Button(this);
        b.setText(R.string.pausebutton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wentOnPause = true;
                startActivityForResult(new Intent(GameActivity.this, PauseMenuActivity.class), 1);
            }
        });
        r.addView(b);
        setContentView(r);

        mv = new MusicManager(this);
        sharedPreferences = getBaseContext().getSharedPreferences("sp", MODE_PRIVATE);
        if (sharedPreferences.getBoolean("music",true)) {
            mv.launchMusic();
        }
    }

    public static GameActivity getInstance(){
        return gameActivity;
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensor.onResume();
        gameControler.startGame();

        if (sharedPreferences.getBoolean("music",true)) {
            if (mv.isStopped()){
                mv.launchMusic();
            }
            else mv.playSound();
        }
        else {
            mv.stopSound();
        }
        if (wentOnPause){
            startActivityForResult(new Intent(GameActivity.this, PauseMenuActivity.class), 1);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameControler.stopGame();
        sensor.onPause();
        //gv.pause();
        mv.pauseSound();
        wentOnPause = true;
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            wentOnPause=false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onGameFinished() {
        finish();
        startActivity(new Intent(this, EndGameActivity.class));
    }


}
