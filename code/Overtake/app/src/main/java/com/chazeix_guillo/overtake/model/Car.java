package com.chazeix_guillo.overtake.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;

public class Car {
    private Bitmap bitmapSprite;
    private Rect car;

    public Car(int x, int y, int sprite, Context context) {
        createBitmap(sprite,context);
        car = new Rect(x,y,bitmapSprite.getWidth(),bitmapSprite.getHeight());

    }

    public Car(int sprite, Context context){
        createBitmapInverse(sprite,context);
        car = new Rect();
        car.right = bitmapSprite.getWidth();
        car.bottom = bitmapSprite.getHeight();
    }

    private void createBitmap(int sprite, Context context) {
        bitmapSprite = BitmapFactory.decodeResource(context.getResources(), sprite);
        float ratio = (float) bitmapSprite.getWidth() / (float) bitmapSprite.getHeight();
        int newWidth = bitmapSprite.getWidth() / 3;
        int newHeight = Math.round(newWidth / ratio);
        bitmapSprite = Bitmap.createScaledBitmap(bitmapSprite, newWidth, newHeight, true);
    }

    private void createBitmapInverse(int sprite, Context context) {
        Matrix m = new Matrix();
        m.postRotate(180);
        bitmapSprite = BitmapFactory.decodeResource(context.getResources(), sprite);
        float ratio = (float) bitmapSprite.getWidth() / (float) bitmapSprite.getHeight();
        int newWidth = bitmapSprite.getWidth() / 3;
        int newHeight = Math.round(newWidth / ratio);
        bitmapSprite = Bitmap.createScaledBitmap(bitmapSprite, newWidth, newHeight, true);
        bitmapSprite = Bitmap.createBitmap(bitmapSprite, 0, 0, bitmapSprite.getWidth(), bitmapSprite.getHeight(), m, true);
    }

    public float getX() {
        return car.left;
    }

    public synchronized void changeX(int shift) {
        car.left += shift;
    }

    public synchronized void changeY(int shift){
        car.top += shift;
    }

    public float getY() {
        return car.top;
    }

    public Bitmap getBitmapSprite() {
        return bitmapSprite;
    }

    public synchronized void setCoord(double x){
        car.left = (int)x;
        car.top = -bitmapSprite.getHeight();
    }

    public Rect getCar(){
        return car;
    }
}
