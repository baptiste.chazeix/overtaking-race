package com.chazeix_guillo.overtake.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.chazeix_guillo.overtake.R;

public class PauseMenuActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    Switch musicOnOff;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pause_menu);
        sharedPreferences = getBaseContext().getSharedPreferences("sp", MODE_PRIVATE);
        musicOnOff=findViewById(R.id.musicOnOff);
        if (sharedPreferences.getBoolean("music",true)) {
            musicOnOff.setChecked(true);
        } else {
            musicOnOff.setChecked(false);
        }
        musicOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if (isChecked){
                    sharedPreferences
                            .edit()
                            .putBoolean("music",true)
                            .apply();
                }
                else {
                    sharedPreferences
                            .edit()
                            .putBoolean("music",false)
                            .apply();
                }
            }
        });
    }

    public void continue_playing(View view) {
        setResult(1);
        finish();
    }

    public void replay(View view) {
        GameActivity.getInstance().finish();
        startActivity(new Intent(this, GameActivity.class));
    }

    public void cancel_game(View view) {
        GameActivity.getInstance().finish();
        finish();
    }


}
