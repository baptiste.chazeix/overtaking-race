package com.chazeix_guillo.overtake.interfaces;

import android.graphics.Bitmap;

import com.chazeix_guillo.overtake.model.Car;

import java.util.List;

public interface IGameControler {
    Bitmap getSpritePlayer();
    float getXPlayer();
    float getYPlayer();
    List<Car> getVisibleCars();
    int getCurrentScore();
}
