package com.chazeix_guillo.overtake.controler;

import android.content.Context;
import android.media.MediaPlayer;

import com.chazeix_guillo.overtake.R;


public class MusicManager {

    private MediaPlayer mp=null;
    private Context context;
    private boolean stopped;

    public MusicManager(Context context){
        this.context=context;
    }

    private void playASound(int resid){
        mp = MediaPlayer.create(context, resid);
        mp.setLooping(true);
        mp.start();
        stopped = false;
    }

    public void launchMusic(){
        playASound(R.raw.game_music);
    }

    public void pauseSound(){
        if (mp!=null) {
            mp.pause();
        }
    }

    public void playSound() {
        if (mp!=null) {
            mp.start();
        }
    }

    public void stopSound(){
        if (mp!=null) {
            mp.stop();
            stopped = true;
        }
    }

    public boolean isStopped(){
        return stopped;
    }
}
