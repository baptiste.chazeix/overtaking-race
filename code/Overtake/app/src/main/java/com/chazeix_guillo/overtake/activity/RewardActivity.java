package com.chazeix_guillo.overtake.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.chazeix_guillo.overtake.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RewardActivity extends AppCompatActivity {

    ListView list_scores;
    TextView message;
    List<String> list;
    ArrayAdapter<String> adapt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reward);
        list_scores = findViewById(R.id.list_scores);
        message = findViewById(R.id.message_scores);
        SharedPreferences sp = getSharedPreferences("score",MODE_PRIVATE);
        if(sp.contains("list_scores")){
            Set<String> set = sp.getStringSet("list_scores",null);
            if(set.isEmpty()) {
                list = new ArrayList<>();
                adapt = new ArrayAdapter<>(RewardActivity.this, android.R.layout.simple_list_item_1, list);
                list_scores.setVisibility(View.GONE);
                message.setVisibility(View.VISIBLE);
            }
            else {
                list = new ArrayList<>(set);
                adapt = new ArrayAdapter<>(RewardActivity.this, android.R.layout.simple_list_item_1, list);
                message.setVisibility(View.GONE);
                list_scores.setVisibility(View.VISIBLE);
            }
        }
        else {
            list = new ArrayList<>();
            adapt = new ArrayAdapter<>(RewardActivity.this, android.R.layout.simple_list_item_1, list);
            list_scores.setVisibility(View.GONE);
            message.setVisibility(View.VISIBLE);

        }
        list_scores.setAdapter(adapt);
    }
}
