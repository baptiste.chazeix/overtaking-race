package com.chazeix_guillo.overtake.model;

import android.content.Context;

import com.chazeix_guillo.overtake.R;

import java.util.ArrayList;
import java.util.List;

public class CarBag {
    private List<Car> bag;

    public CarBag(Context context) {
        bag = new ArrayList<>();
        defineCars(context);
    }

    private void defineCars(Context context) {
        bag.add(new Car(R.drawable.black_viper, context));
        bag.add(new Car(R.drawable.audi, context));
        bag.add(new Car(R.drawable.ambulance, context));
        bag.add(new Car(R.drawable.mini_truck, context));
        bag.add(new Car(R.drawable.mini_van, context));
        bag.add(new Car(R.drawable.mustang, context));
        bag.add(new Car(R.drawable.taxi, context));
    }

    public int getSizeBag(){
        return bag.size();
    }

    public Car getCar(int index){
        return bag.get(index);
    }
}
