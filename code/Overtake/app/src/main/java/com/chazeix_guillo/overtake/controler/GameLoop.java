package com.chazeix_guillo.overtake.controler;

import com.chazeix_guillo.overtake.interfaces.IGameControlerListener;

public class GameLoop extends Thread{

    private IGameControlerListener gc;
    private boolean running = false;

    public GameLoop(IGameControlerListener gc) {
        this.gc = gc;
    }

    @Override
    public void run() {
        while(running){
            try {
                long time = System.currentTimeMillis();
                gc.update();
                long waitTime = (time + 16) - System.currentTimeMillis();
                if(waitTime < 0) {
                    waitTime = 0;
                }
                sleep(waitTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void startRunning(){
        running = true;
    }

    public void stopRunning(){
        running = false;
    }
}
