package com.chazeix_guillo.overtake.controler;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.WindowManager;

import com.chazeix_guillo.overtake.R;
import com.chazeix_guillo.overtake.interfaces.GameListener;
import com.chazeix_guillo.overtake.interfaces.IGameControler;
import com.chazeix_guillo.overtake.interfaces.IGameControlerListener;
import com.chazeix_guillo.overtake.interfaces.IGameView;
import com.chazeix_guillo.overtake.model.Car;
import com.chazeix_guillo.overtake.model.CarBag;
import com.chazeix_guillo.overtake.model.Score;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class GameControler implements IGameControlerListener, IGameControler {

    private Context context;
    private SensorControler sensor;
    private IGameView iGameView;
    private GameLoop gameLoop;
    private GameListener gameListener;

    private Car player;
    private CarBag bag;
    private List<Car> visibleCars;
    private Score score;

    private Point p;

    private double minX;
    private double maxX;

    private double left;
    private double leftCenter;
    private double rightCenter;
    private double right;
    private int speed = 4;
    private int spawn = 65;

    private int timeToSpawn = 0;

    public GameControler(Context context, SensorControler sensor, GameListener gameListener) {
        this.sensor = sensor;
        this.context = context;
        this.gameListener = gameListener;
        p = new Point();
        WindowManager wm = (WindowManager)context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getSize(p);

        calculRatio();
        player = new Car(p.x / 2,p.y - p.y / 3, R.drawable.police, context);
        bag = new CarBag(context);
        visibleCars = Collections.synchronizedList(new ArrayList<Car>());
        score = new Score();
    }

    private void calculRatio() {
        maxX = p.x * 0.74;
        minX = p.x * 0.12;

        left = p.x * 0.17;
        leftCenter = p.x * 0.34;
        rightCenter = p.x * 0.515;
        right = p.x * 0.69;
    }

    @Override
    public void update(){
        /* difficulty */
        if(getCurrentScore() % 500 == 0){
            if(speed != 24 && spawn != 15){
                speed += 2;
                spawn -= 5;
            }
        }

        positionChangedForPlayer(sensor.getDirection());
        positionChangedForCars();
        timeToSpawn++;


        if(timeToSpawn == spawn){
            Car c = getRandomCar();
            if(!visibleCars.contains(c)) {
                c.setCoord(randomX());
                visibleCars.add(c);
            }
            timeToSpawn = 0;
        }

        for(int i=0;i<visibleCars.size();i++){
            if(visibleCars.get(i).getY() >= p.y) {
                visibleCars.remove(i);
                i--;
            }
        }

        /* increase score */
        score.incrementScore();

        /* draw game */
        iGameView.draw();

        /* collision */
        for(Car c: visibleCars){
            if(player.getCar().left < (c.getCar().left + c.getCar().right) && c.getCar().left < (player.getCar().left + player.getCar().right) && player.getCar().top < (c.getCar().top + c.getCar().bottom) && c.getCar().top < (player.getCar().top + player.getCar().bottom)) {
                SharedPreferences sp = context.getSharedPreferences("score",Context.MODE_PRIVATE);
                if(sp.contains("list_scores")){
                    score.setScores(sp.getStringSet("list_scores",null));
                }
                score.addCurrentScore();
                sp.edit().putStringSet("list_scores",score.getScores()).apply();
                sp.edit().putInt("current_score",score.getCurrentScore()).apply();
                gameListener.onGameFinished();
            }
        }
    }

    private double randomX() {
        Random r = new Random();
        switch (r.nextInt(4)){
            case 0: return left;
            case 1: return leftCenter;
            case 2: return rightCenter;
            case 3: return right;
            default: return left;
        }
    }

    private void positionChangedForCars() {
        for(int i=0;i<visibleCars.size();i++){
            visibleCars.get(i).changeY(speed);
        }
    }

    @Override
    public int getCurrentScore() {
        return score.getCurrentScore();
    }

    public void stopGame() {
        gameLoop.stopRunning();
    }

    public void startGame(){
        gameLoop = new GameLoop(this);
        gameLoop.startRunning();
        gameLoop.start();
    }

    public void setGameView(IGameView gv) {
        iGameView = gv;
    }

    private void positionChangedForPlayer(Integer direction){
        if(direction == null)
            return;
        if(direction == 2){
            if(player.getX() >= maxX)
                return;
            player.changeX(8);
        }
        else if(direction == 1) {
            if (player.getX() <= minX)
                return;
            player.changeX(-8);
        }
    }

    @Override
    public Bitmap getSpritePlayer(){
        return player.getBitmapSprite();
    }

    public float getXPlayer(){
        return player.getX();
    }

    public float getYPlayer(){
        return player.getY();
    }

    @Override
    public List<Car> getVisibleCars() {
        return visibleCars;
    }

    private Random r = new Random();

    private Car getRandomCar(){
        return bag.getCar(r.nextInt(bag.getSizeBag()));
    }
}
