package com.chazeix_guillo.overtake.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.View;

import com.chazeix_guillo.overtake.R;
import com.chazeix_guillo.overtake.interfaces.IGameControler;
import com.chazeix_guillo.overtake.interfaces.IGameView;
import com.chazeix_guillo.overtake.model.Car;


public class GameView extends View implements IGameView {
    private boolean isCreated = false;

    private Paint paint;
    private Context context;

    private Bitmap background1;
    private Bitmap background2;

    private int backgroundY1;
    private int backgroundY2;

    private IGameControler igc;

    public GameView(Context context, IGameControler gc) {
        this(context,null, gc);
    }

    public GameView(Context context, AttributeSet attrs, IGameControler gc) {
        super(context, attrs);
        igc = gc;
        this.context = context;
        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(50);
    }

    @Override
    public void draw() {
        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(getWidth() != 0){ // Verifie si la vue est prête.
            if(!isCreated) {
                createBitmaps();
            }
        }
        if(canvas != null) {
            canvas.drawColor(0, PorterDuff.Mode.CLEAR);

            canvas.drawBitmap(background1, 0, backgroundY1, paint);
            canvas.drawBitmap(background2,0,backgroundY2,paint);
            backgroundY1 += 25;
            backgroundY2 += 25;
            if(backgroundY1 >= getHeight()) {
                backgroundY1 = -background1.getHeight() + backgroundY2;
            }
            else if(backgroundY2 >= getHeight()) {
                backgroundY2 = -background2.getHeight() + backgroundY1;
            }

            drawAllCars(canvas);

            canvas.drawBitmap(igc.getSpritePlayer(), igc.getXPlayer(), igc.getYPlayer(), paint);

            paint.setColor(Color.BLACK);
            paint.setAlpha(150);
            canvas.drawRect(0,0,getWidth(),(float)(getHeight()/12),paint);

            paint.setColor(Color.WHITE);
            canvas.drawText(Integer.toString(igc.getCurrentScore()), (float)(getWidth()-getWidth()/4),100f,paint);
        }
    }

    private void drawAllCars(Canvas canvas) {
        for(int i=0;i<igc.getVisibleCars().size();i++){
            Car car = igc.getVisibleCars().get(i);
            canvas.drawBitmap(car.getBitmapSprite(),car.getX(),car.getY(), paint);
        }
    }

    private void createBitmaps() {



        background1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.street);
        float ratio = (float) background1.getWidth() / (float) background1.getHeight();
        int newHeight = Math.round(getWidth() / ratio);
        background1 = Bitmap.createScaledBitmap(background1, getWidth(), newHeight, true);
        background2 = background1.copy(Bitmap.Config.RGB_565,false);

        backgroundY1 = -background1.getHeight() + getHeight();
        backgroundY2 = -background2.getHeight() - (background1.getHeight() - getHeight());

        isCreated = true;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }
}
