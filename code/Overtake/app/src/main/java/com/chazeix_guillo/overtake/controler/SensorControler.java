package com.chazeix_guillo.overtake.controler;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorControler implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor sensor;

    private Integer direction = 0;

    public SensorControler(SensorManager sensorManager){
        this.sensorManager = sensorManager;
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public Integer getDirection() {
        return direction;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.values[0] >= 1.5f){
            direction = 1;
        }
        else if(event.values[0] <= -1.7f){
            direction = 2;
        }
        else
            direction = 0;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onResume() {
        sensorManager.registerListener(this,sensor,SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void onPause() {
        sensorManager.unregisterListener(this);
    }

}
