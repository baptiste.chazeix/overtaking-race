package com.chazeix_guillo.overtake.interfaces;

public interface GameListener {
    void onGameFinished();

}
