package com.chazeix_guillo.overtake.model;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class Score {
    private int currentScore;
    private LinkedList<String> scores;

    public Score() {
        currentScore = 0;
        scores = new LinkedList<>();
    }

    public Set<String> getScores() {
        return new LinkedHashSet<>(scores);
    }

    public void addCurrentScore(){
        scores.addFirst(Integer.toString(currentScore));
    }

    public void incrementScore() {
        currentScore++;
    }

    public void setScores(Set<String> scores) {
        this.scores = new LinkedList<>(scores);
    }

    public int getCurrentScore() {
        return currentScore;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
