package com.chazeix_guillo.overtake.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.chazeix_guillo.overtake.R;

public class EndGameActivity extends AppCompatActivity {

    TextView tv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.endgame);

        tv = findViewById(R.id.score);
        int score;
        SharedPreferences sp = getSharedPreferences("score", Context.MODE_PRIVATE);
        if(sp.contains("current_score")) {
            score = sp.getInt("current_score", 0);
            tv.setText(Integer.toString(score));
        }
        else tv.setText(R.string.error_score);
    }
}
